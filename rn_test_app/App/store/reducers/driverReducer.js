import {GET_DRIVER} from '../types';

const initialState = {
    info: {}
}

export const driverReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DRIVER:
            return {
                ...state,
                info: action.payload
            }
        default:
            return state
    }
}
