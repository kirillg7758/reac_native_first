import {CHANGE_ROUND, CHANGE_SEASON, CHANGE_SERIES} from '../types';

const initState = {
    series: ['series'],
    seasons: [],
    rounds: []
}

export const selectsReducer = (state = initState, action) => {
    switch (action.type) {
        case CHANGE_SERIES:
            return {
                ...state,
                series: action.payload || ['series', 'F1']
            }
        case CHANGE_SEASON:
            return {
                ...state,
                seasons: action.payload
            }
        case CHANGE_ROUND:
            return {
                ...state,
                rounds: action.payload
            }
        default:
            return state
    }
}
