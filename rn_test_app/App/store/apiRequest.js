import {asyncAction} from './actions/asyncAction';
import {CHANGE_ROUND, CHANGE_SEASON} from './types';

export class ApiRequest {
    static getDriver = async (payload) => {
        const res = await fetch(`http://ergast.com/api/f1/drivers/${payload}.json`, {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        })
        const {MRData: {DriverTable}} = await res.json()
        const [Driver] = DriverTable.Drivers
        return Driver
    }

    static getDriverStanding = async (year, round) => {
        const res = await fetch(`http://ergast.com/api/f1/${year}/${round}/driverStandings.json`, {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        })
        const {MRData: {StandingsTable}} = await res.json()
        return [...StandingsTable.StandingsLists[0].DriverStandings]
    }

    static filterTable = async (payload) => {
        const res = await fetch(
            `http://ergast.com/api/f1/${payload.season}/${payload.round}/drivers.json?limit=10&offset=${payload.page * 5}`, {
                method: 'GET',
                headers: {'Content-Type': 'application/json'}
            })
        const {MRData: {DriverTable}} = await res.json()
        return DriverTable.Drivers
    }

    static changeSeason = async (dispatch) => await asyncAction(dispatch, {
        url: 'http://ergast.com/api/f1.json',
        method: 'GET',
        needData: 'season',
        action: CHANGE_SEASON
    })

    static changeRound = async (dispatch, payload) => await asyncAction(dispatch, {
        url: `http://ergast.com/api/f1/${payload}.json`,
        method: 'GET',
        needData: 'round',
        action: CHANGE_ROUND
    })
}
