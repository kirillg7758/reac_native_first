export const CHANGE_SERIES = 'CHANGE_SERIES'
export const CHANGE_SEASON = 'CHANGE_SEASON'
export const CHANGE_ROUND = 'CHANGE_ROUND'

export const GET_DRIVERS = 'GET_DRIVERS'
export const GET_DRIVER = 'GET_DRIVER'
export const GET_DRIVER_Standing = 'GET_DRIVER_Standing'
export const CHANGE_PAGE = 'CHANGE_PAGE'

export const GET_ERR = 'GET_ERR'
export const CLEAN_ERR = 'CLEAN_ERR'
