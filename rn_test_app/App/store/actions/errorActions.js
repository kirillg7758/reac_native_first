import {CLEAN_ERR} from '../types';

export const clearErrAction = () => {
    return {
        type: CLEAN_ERR
    }
}
