import React from 'react'
import {Picker, StyleSheet} from 'react-native';
import {useDispatch} from 'react-redux';

export const AppPicker = ({valuePicker, setValue, pickerItem, firstPicker, action, actionPayload }) => {
    const dispatch = useDispatch()
    return(
        <Picker
            selectedValue={valuePicker}
            style={[styles.Picker]}
            itemStyle={styles.PickerItem}
            onValueChange={(itemValue, itemIndex) => {
                setValue(itemValue)
                if (action) {
                    const payload = actionPayload && {
                        ...actionPayload,
                        round: itemValue
                    }

                    dispatch(action(payload || itemValue))
                }
            }}
        >
            <Picker.Item label={firstPicker.label} value={firstPicker.value} />
            {pickerItem && pickerItem.map((item, idx) =>
                <Picker.Item key={'picker-' + item.value + '-' + idx} label={item} value={item} />)}
        </Picker>
    )
}

const styles = StyleSheet.create({
    Picker: {
        height: 50,
        width: '30%'
    },
    PickerItem: {
        height: 40,
        fontSize: 14,
    }
});
