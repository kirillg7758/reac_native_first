import React, {useEffect} from 'react'
import {StyleSheet, View, FlatList} from 'react-native'
import {shallowEqual, useDispatch, useSelector} from 'react-redux';

import {AppCart} from '../components/AppCard/AppCard';
import {getDriverStandingAction} from '../store/actions/getDriverStandingAction';

export const DriverStandingsScreen = ({route, navigation}) => {
    const {table} = useSelector(({driverStanding}) => driverStanding , shallowEqual)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getDriverStandingAction(route.params))
    },[])

    return(
        <View style={styles.wrapper}>
            <AppCart pos='Pos' name='Tracer Name' age='Age' wins='Wins' className={styles.cart} />
            <FlatList
                data={table}
                renderItem={({item}) =>
                    <AppCart
                        navigation={navigation}
                        driverID={item.Driver.driverId}
                        pos={item.position}
                        wins={item.wins}
                        name={item.Driver.familyName}
                        age={new Date().getFullYear() - new Date(item.Driver.dateOfBirth).getFullYear()}
                        className={styles.cart}
                    />}
                keyExtractor={item => item.position}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 20
    },
    cart: {
        fontSize: 14
    }
})
